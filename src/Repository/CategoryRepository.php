<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Category>
 *
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function save(Category $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Category $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    /**
     * @return Category[]
     */
    public function findByAllOrdered(): array
    {
        /* $dql= 'SELECT category FROM App\Entity\Category as category ORDER BY category.name DESC';
        $query= $this->getEntityManager()->createQuery($dql); */
        // le createQueryBuilder reprend : SELECT category FROM App\Entity\Category as category
        // c'est l'alias category qu'on retrouve entre parenthèse de la méthode querybuilder
        $qb = $this->createQueryBuilder('category')
        ->addOrderBy('category.name', Criteria::DESC);
        $query = $qb->getQuery();

        return $query->getResult();

    }
    /**
     * @return Category[]
     */
    public function search(string $term): array
    {
        return $this->createQueryBuilder('category')
            // en ajoutant le addSelect, cela empeche doctrine de faire deux requête (une première pour récupérer les données de la table catégorie puis celle de FortuneCookies)
            // il y a deux selects, category et fortuneCookie
            // quand on est ds le cas search
            ->addSelect('fortuneCookie')
            ->leftJoin('category.fortuneCookies', 'fortuneCookie')
            // c'est plus pratique de mettre un andWhere dès le début qu'un where parce que
            /* Qu'est-ce qui ne va pas avec ->where() ? Eh bien, si vous avez ajouté une clause WHERE à votre QueryBuilder plus tôt, 
            appeler ->where() supprimerait cela et le remplacerait par les nouvelles choses... 
            ce qui probablement n'est pas ce que vous voulez. ->andWhere() ajoute toujours à la requête. */
            // c'est plus pratique aussi de mettre le Or dans le and directement
            ->andWhere('category.name LIKE :searchTerm OR category.iconKey LIKE :searchTerm OR fortuneCookie.fortune LIKE :searchTerm')
            ->setParameter('searchTerm', '%'.$term.'%')
            ->addOrderBy('category.name', Criteria::DESC)
            ->getQuery()
            ->getResult();
    }
    public function findWithFortunesJoin(int $id): ?Category
    {
        return $this->createQueryBuilder('category')
            ->addSelect('fortuneCookie')
            ->leftJoin('category.fortuneCookies', 'fortuneCookie')
            ->andWhere('category.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
