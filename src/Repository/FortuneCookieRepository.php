<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\FortuneCookie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FortuneCookie>
 *
 * @method FortuneCookie|null find($id, $lockMode = null, $lockVersion = null)
 * @method FortuneCookie|null findOneBy(array $criteria, array $orderBy = null)
 * @method FortuneCookie[]    findAll()
 * @method FortuneCookie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FortuneCookieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FortuneCookie::class);
    }

    public function save(FortuneCookie $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(FortuneCookie $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function countNumberPrintedForCategory(Category $category): array
    {
        $result = $this->createQueryBuilder('fortuneCookie')
            ->select('SUM(fortuneCookie.numberPrinted) AS fortunesPrinted')
            ->addSelect('AVG(fortuneCookie.numberPrinted) fortunesAverage')
            ->addSelect('category.name')
            ->innerJoin('fortuneCookie.category', 'category')
            ->andWhere('fortuneCookie.category = :category')
            ->setParameter('category', $category)
            ->groupBy('category.name') 
            ->getQuery()
            ->getSingleResult();
            // permet d'avoir le résultat directement au lieu de faire $result['fortunesPrinted']
            // marche que si on retourne un seul résultat, une seule colonne
            //->getSingleScalarResult();
        return $result;
    }
}
